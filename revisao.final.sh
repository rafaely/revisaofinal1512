#!/bin/bash

file_name="access.log"
url="https://raw.githubusercontent.com/linuxacademy/content-elastic-log-samples/master/access.log"

if [ ! -e access.log ]; then
    echo "Fazendo o download do arquivo..."
    wget -O access.log $url
fi
RE='^[^ ]+'
grep -E -o "$RE" access.log | sort -n -u

awk '{count[$1]++}  END {for (ip in count) print ip, count[ip] < access.log}'| sort -n -r -k 2
sed -r "s/$RE/SUPRIMIDO/g" < access.log

oc="[0-2]?[0-9]{1,2}"
RE="($oc)\.($oc)\.($oc)\.($oc)"
sed -r "s/$RE/x.\2.\3.x/g" < access.log
